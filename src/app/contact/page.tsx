import { PodcastProviderCard } from "@/components/widgets/cards";
import { ContactForm } from "@/components/widgets/contact";
import { pageCache } from "@/services/page_cache";
import { renderFileUrl } from "@/utils";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

const ContactPage: FC = async () => {
  const { podcastAccounts, socialAccounts } = await pageCache.contact();

  return (
    <>
      <main
        className="container mx-auto px-2 relative pt-10 md:pt-24"
        style={{ minHeight: "calc(100vh - 5rem)" }}
      >
        <div className="w-full flex flex-col md:flex-row items-start justify-around gap-10">
          <div className="w-full max-w-[624px]">
            <div className="flex flex-col gap-4 ">
              <div className="heading2">Contact Us</div>
              <div className="heading4 text-primary">
                Interested in collaborating? Please fill the form below.
              </div>
            </div>
            <ContactForm />
          </div>
          <div className="w-full max-w-[468px]">
            <div>
              <div className="heading4 text-grey mb-4">Subscribe</div>
              <div className="grid grid-cols-2 gap-4">
                {podcastAccounts.map((item) => (
                  <Link key={item.name} href={item.url} target="_blank">
                    <PodcastProviderCard podcastAccount={item} />
                  </Link>
                ))}
              </div>
            </div>
            <div className="mt-10 pb-10">
              <div className="heading4 text-grey mb-4">Socials</div>
              <div className="flex flex-row items-center gap-4">
                {socialAccounts?.map((item) => (
                  <div key={item.name}>
                    <Link href={item.url} target="_blank">
                      <Image
                        src={renderFileUrl(item.logo_path)}
                        alt="social_logo"
                        width={25}
                        height={25}
                      />
                    </Link>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className="absolute bottom-0 right-48 h-[320px] w-[280px] -z-10 hidden md:block">
          <Image
            src={"/images/contactus_lines.png"}
            alt="lines"
            fill
            style={{ objectFit: "contain" }}
          />
        </div>
      </main>
    </>
  );
};

export default ContactPage;
