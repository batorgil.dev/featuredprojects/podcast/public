import { Footer, Sponsers, Subscription } from "@/components/layout";
import { AvailableOn } from "@/components/widgets/cards";
import {
  RecentEpisodes,
  RelatedBlogs,
  Reviews,
} from "@/components/widgets/containers";
import { HeroSection } from "@/components/widgets/home";
import { pageCache } from "@/services/page_cache";
import Image from "next/image";
import { FC } from "react";

export const revalidate = 3600;

const HomePage: FC = async () => {
  const { reviews, recentPodcasts, podcastAccounts, recentBlogs } =
    await pageCache.home();

  return (
    <>
      <main className="container mx-auto px-2">
        {/* hero section */}
        <HeroSection />
        <AvailableOn podcastAccounts={podcastAccounts} />
        <div className="flex flex-col-reverse md:flex-row md:items-stretch  gap-4 md:justify-between relative mt-24">
          <div className="flex-1 relative flex justify-center items-center">
            <div className="max-w-[410px] w-full flex flex-col gap-6 relative ">
              <div className="absolute w-full h-full">
                <div className="bg-primary absolute -top-10 h-[8px] w-[20px]"></div>
                <div className="bg-primary absolute -top-20 -right-10 h-[8px] w-[56px]"></div>
                <div className="bg-secondary absolute -bottom-10 -left-10 h-[8px] w-[56px]"></div>
              </div>
              <h1 className="heading2">
                A podcast for makers and entrepreneurs
              </h1>
              <div>
                Join us for insightful conversations with makers and
                entrepreneurs. Gain valuable tips and inspiration for your
                creative ventures.
              </div>
            </div>
          </div>
          <div className="flex-1 relative min-h-[280px] md:min-h-[490px]">
            <Image
              src={"/images/hero1.png"}
              alt="hero"
              fill
              style={{ objectFit: "contain" }}
            />
          </div>
        </div>
        <div className="flex flex-col-reverse md:flex-row md:items-stretch  gap-4 md:justify-between relative mt-24">
          <div className="flex-1 relative min-h-[280px] md:min-h-[490px]">
            <Image
              src={"/images/hero2.png"}
              alt="hero"
              fill
              style={{ objectFit: "contain" }}
            />
          </div>
          <div className="flex-1 relative flex justify-center items-center">
            <div className="max-w-[410px] w-full flex flex-col gap-6 relative ">
              <div className="absolute w-full h-full">
                <div className="bg-secondary absolute -top-20 -right-10 h-[8px] w-[56px]"></div>
              </div>
              <h1 className="heading2">
                My origins on <span className="text-primary">YouTube</span> in
                2008
              </h1>
              <div>
                Podcasting emerged in the early 2008s, driven by the
                democratization of media production and distribution. It rapidly
                evolved from niche hobbyists to a mainstream form of on-demand
                audio content, shaping modern media consumption and fostering
                diverse voices and communities worldwide.
              </div>
            </div>
          </div>
        </div>
        <RecentEpisodes podcasts={recentPodcasts} />
        <Reviews reviews={reviews} />
        <div className="mt-24" />
        <Subscription />
        <div className="mt-24" />
        <RelatedBlogs blogs={recentBlogs} />
      </main>
      <Footer podcastProviders={podcastAccounts}>
        <Sponsers />
      </Footer>
    </>
  );
};

export default HomePage;
