import { Footer, Subscription } from "@/components/layout";
import { AllBlogs } from "@/components/widgets/containers";
import { pageCache } from "@/services/page_cache";
import { renderFileUrl } from "@/utils";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

const BlogsPage: FC = async () => {
  const { featuredBlog, podcastAccounts } = await pageCache.blogs();
  return (
    <>
      <main className="container mx-auto px-2">
        {/* hero section */}
        {!!featuredBlog && (
          <div className="md:h-[462px] flex flex-col-reverse md:flex-row gap-16 items-center pt-24">
            <div className="flex-1 w-full flex flex-col gap-4 m-auto">
              <p className="caption text-primary">Featured</p>
              <h1 className="heading1 line-clamp-2">{featuredBlog.title}</h1>
              <p>{featuredBlog.summary}</p>
              <div className="flex flex-col md:flex-row">
                <Link
                  className="text-primary see-more relative flex flex-row gap-2 items-center"
                  href={`/blogs/${featuredBlog.id}`}
                >
                  <div>Read Now</div>
                  <div className="relative w-[23px] h-[12px]">
                    <Image
                      src={"/images/arrow_stroke.png"}
                      alt="arrow"
                      fill
                      style={{ objectFit: "contain" }}
                    />
                  </div>
                </Link>
              </div>
            </div>
            <div className="relative flex-1 w-full h-full flex justify-end">
              <div className="h-full w-full relative">
                <Image
                  src={renderFileUrl(featuredBlog.cover_path)}
                  alt="hero"
                  fill
                  style={{ objectFit: "cover" }}
                />
              </div>
            </div>
          </div>
        )}

        <AllBlogs />
      </main>
      <Footer podcastProviders={podcastAccounts}>
        <Subscription />
      </Footer>
    </>
  );
};

export default BlogsPage;
