import { Footer, Subscription } from "@/components/layout";
import { Error404 } from "@/components/ui";
import { Author } from "@/components/widgets";
import { RelatedBlogs } from "@/components/widgets/containers";
import { pageCache } from "@/services/page_cache";
import { renderDateEnglish, renderFileUrl } from "@/utils";
import Image from "next/image";
import { FC } from "react";

const SingleSinglePage: FC<{
  params: { id: string };
}> = async ({ params: { id } }) => {
  const { detail, podcastAccounts, relatedBlogs } = await pageCache.blogSingle(
    id
  );
  return (
    <>
      <main className="container px-2 pt-24 mx-auto">
        {!!detail && (
          <>
            <div className="max-w-[1060px] mx-auto flex flex-col gap-4">
              <p className="caption text-primary">
                {renderDateEnglish(detail.published_at)}
              </p>
              <h1 className="heading2">{detail.title}</h1>
            </div>
            <div className="w-full h-full min-h-[450px] relative mt-16">
              <Image
                src={renderFileUrl(detail.cover_path)}
                alt="hero"
                fill
                style={{ objectFit: "cover" }}
              />
            </div>
            <div className="flex flex-col-reverse md:flex-row gap-4 md:justify-between relative mt-10 max-w-[1060px] mx-auto">
              <div className="flex-1 w-full pr-10">
                <div
                  dangerouslySetInnerHTML={{ __html: detail.description_html }}
                ></div>
                <Author author={detail.author} />
              </div>
            </div>
          </>
        )}
        {!detail && (
          <>
            <Error404 />
          </>
        )}

        <div className="mt-24" />
        <RelatedBlogs blogs={relatedBlogs} />
      </main>
      <Footer podcastProviders={podcastAccounts}>
        <Subscription />
      </Footer>
    </>
  );
};

export default SingleSinglePage;
