import { Footer, Sponsers } from "@/components/layout";
import { AvailableOn, ContactUsCard } from "@/components/widgets/cards";
import { RecentEpisodes } from "@/components/widgets/containers";
import { HostStatistics } from "@/components/widgets/host";
import { pageCache } from "@/services/page_cache";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

const HostPage: FC = async () => {
  const { podcastAccounts, recentPodcasts, statistics } =
    await pageCache.host();

  return (
    <>
      <main className="container mx-auto px-2">
        {/* hero section */}
        <div className="md:h-[60vh] flex flex-col md:flex-row gap-16 max-w-[1024px] items-center mx-auto">
          <div className="relative flex-1 w-full h-[462px] flex justify-end">
            <div className="absolute w-full h-full">
              <div className="absolute w-[24px] h-[8px] bg-primary left-24 top-24" />
              <div className="absolute w-[20px] h-[8px] bg-secondary left-4 bottom-24" />
            </div>
            <div className="h-[462px] w-[340px] relative -right-10">
              <Image
                src={"/images/host.png"}
                alt="hero"
                fill
                style={{ objectFit: "contain" }}
              />
            </div>
          </div>
          <div className="flex-1 w-full flex flex-col gap-8 m-auto">
            <p className="caption text-primary">About The Host</p>
            <h1 className="heading1">
              Hey there, I’m Andrew Jonson and welcome to my Podcast
            </h1>
            <p>
              Finsweet: A creative problem-solver, known for innovation and
              precision. Collaborative, passionate, and inspiring, they push
              boundaries and achieve greatness.
            </p>
            <div className="flex flex-col md:flex-row">
              <Link href={"https://batorgil.dev/"} target="_blank">
                <button className="btn-primary">Know More</button>
              </Link>
            </div>
          </div>
        </div>
        <AvailableOn podcastAccounts={podcastAccounts} />
        <div className="flex flex-col-reverse md:flex-row md:items-stretch  gap-4 md:justify-between relative mt-24">
          <div className="flex-1 relative flex justify-center items-center">
            <div className="max-w-[410px] w-full flex flex-col gap-6 relative ">
              <div className="absolute w-full h-full">
                <div className="bg-primary absolute -bottom-20 -right-10 h-[8px] w-[56px]"></div>
              </div>
              <h1 className="heading2">A small story about a big dream —</h1>
              <div>
                In a small studio, Finsweet nurtured a big dream. With
                dedication and creativity, they built a channel to empower
                others. Through challenges and triumphs, their vision grew,
                inspiring countless on their journey to success.
              </div>
            </div>
          </div>
          <div className="flex-1 relative min-h-[280px] md:min-h-[490px]">
            <Image
              src={"/images/host1.png"}
              alt="hero"
              fill
              style={{ objectFit: "contain" }}
            />
          </div>
        </div>
        <div className="flex flex-col-reverse md:flex-row md:items-stretch  gap-4 md:justify-between relative mt-24">
          <div className="flex-1 relative flex justify-center items-center">
            <div className="max-w-[410px] w-full flex flex-col gap-6 relative ">
              <div className="absolute w-full h-full">
                <div className="bg-secondary absolute -top-10 -left-10 h-[8px] w-[56px]"></div>
                <div className="bg-primary absolute -bottom-10 h-[8px] w-[20px]"></div>
              </div>
              <h1 className="heading2">
                My origins on <span className="text-primary">YouTube</span> in
                2008
              </h1>
              <div className="flex flex-col gap-4">
                <p>
                  Podcasting emerged in the early 2008s, driven by the
                  democratization of media production and distribution. It
                  rapidly evolved from niche hobbyists to a mainstream form of
                  on-demand audio content, shaping modern media consumption and
                  fostering diverse voices and communities worldwide.
                </p>
                <p>
                  <strong>
                    Started podcasting to share insights, inspire others, and
                    build a community around business success, fostering growth
                    and connections globally.
                  </strong>
                </p>
                <p>
                  Podcasting: a gateway to shared wisdom, boundless creativity,
                  and endless connections, shaping the future of media and
                  communication.
                </p>
              </div>
            </div>
          </div>
          <div className="flex-1 relative flex justify-center items-center">
            <div className="max-w-[410px] w-full flex flex-col gap-6 relative ">
              <div className="absolute w-full h-full">
                <div className="bg-primary absolute -bottom-10 -right-10 h-[8px] w-[70px]"></div>
              </div>
              <h1 className="heading2">
                From tech journalist, to Vlogger and YouTuber
              </h1>
              <div className="flex flex-col gap-4">
                <p>
                  Beginning my journey as a tech journalist, I explored the
                  realm of digital storytelling, captivating audiences with my
                  expertise and insights.
                </p>

                <p>
                  Transitioning to vlogging, I embraced the immersive power of
                  video, fostering community engagement and meaningful
                  connections.
                </p>

                <p>
                  On platforms like YouTube, I continuously adapt to emerging
                  trends, leveraging my passion for technology and storytelling
                  to carve a successful career path.
                </p>
              </div>
            </div>
          </div>
        </div>
        <HostStatistics statistics={statistics} />
        <ContactUsCard bgImageSrc="/images/contact_us.png" />
        <RecentEpisodes podcasts={recentPodcasts} />
      </main>
      <Footer podcastProviders={podcastAccounts}>
        <Sponsers />
      </Footer>
    </>
  );
};

export default HostPage;
