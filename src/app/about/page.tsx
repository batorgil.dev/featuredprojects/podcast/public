import { Footer, Subscription } from "@/components/layout";
import { AboutStatistics } from "@/components/widgets/about";
import { SponserCard, TeamMemberCard } from "@/components/widgets/cards";
import { RecentEpisodes } from "@/components/widgets/containers";
import { pageCache } from "@/services/page_cache";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

const AboutPage: FC = async () => {
  const { podcastAccounts, recentPodcasts, statistics, team, sponsers } =
    await pageCache.about();
  return (
    <>
      <main className="container mx-auto px-2">
        {/* hero section */}
        <div className="md:h-[60vh] flex flex-col-reverse md:flex-row gap-16 max-w-[1024px] items-center mx-auto">
          <div className="flex-1 w-full flex flex-col gap-8 m-auto">
            <h1 className="heading1">About Finsweet Podcast</h1>
            <p>
              Finsweet Podcast: A hub of entrepreneurial insights and
              innovation. Join us as we explore success stories, practical
              strategies, and industry trends, empowering you to thrive in the
              business world.
            </p>
            <div className="flex flex-col md:flex-row">
              <Link href={"/about#subscription"}>
                <button className="btn-primary">Subscribe Now!</button>
              </Link>
            </div>
          </div>
          <div className="relative flex-1 w-full h-[462px] flex justify-end">
            <div className="h-[462px] w-[340px] relative -left-10">
              <Image
                src={"/images/about_us.png"}
                alt="hero"
                fill
                style={{ objectFit: "contain" }}
              />
            </div>
          </div>
        </div>
        <AboutStatistics statistics={statistics} />
        <div className="flex flex-col max-w-[1024px] mx-auto relative mt-24 gap-10">
          <div className="absolute w-full h-full z-10">
            {/* lines */}
            <div className="absolute w-[24px] h-[8px] bg-primary -right-24 top-28" />
            <div className="absolute w-[58px] h-[8px] bg-secondary -left-24 bottom-24" />
          </div>
          <div className="heading2">A few words about our team</div>
          <div className="relative w-full h-[320px]">
            <Image
              src={"/images/about_us_main.png"}
              alt="hero"
              fill
              style={{ objectFit: "cover" }}
            />
          </div>
          <div className="w-full flex flex-col md:flex-row gap-4">
            <div className="flex-1">
              Our team at Finsweet comprises dedicated professionals driven by
              passion and expertise. Collaborative and innovative, we strive for
              excellence in every project, ensuring client satisfaction and
              success.
            </div>
            <div className="flex-1">
              Bringing diverse skills and experiences, we foster a dynamic work
              environment. Our commitment to creativity and continuous learning
              enables us to navigate challenges and deliver impactful solutions.
              As a unified team, we are dedicated to exceeding expectations,
              cultivating lasting relationships, and achieving collective
              success.
            </div>
          </div>
        </div>
        <div className="flex flex-col gap-10 mt-24">
          <div className="heading2">Meet Our Team</div>
          <div className="grid grid-cols-1 md:grid-cols-4 gap-4">
            {team.map((item, index) => (
              <TeamMemberCard key={index} teamMember={item} />
            ))}
          </div>
        </div>
        <div className="flex flex-col gap-10 mt-24">
          <div className="heading2">Our Sponsors</div>
          <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
            {sponsers.map((item) => (
              <SponserCard key={item.logo_path} sponser={item} />
            ))}
            <Link href={"/contact"}>
              <div className="flex flex-col justify-between items-start h-[224px] bg-primary text-white p-8">
                <div className="heading2 max-w-[208px]">Become a Sponsor</div>
                <div className="relative bottom-4">
                  <div>Contact Now</div>
                  <div className="absolute w-[58px] h-[8px] bg-secondary left-0 -bottom-4" />
                </div>
              </div>
            </Link>
          </div>
        </div>
        <RecentEpisodes podcasts={recentPodcasts} />
      </main>
      <Footer podcastProviders={podcastAccounts}>
        <Subscription />
      </Footer>
    </>
  );
};

export default AboutPage;
