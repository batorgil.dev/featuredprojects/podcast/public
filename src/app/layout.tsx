import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { Header } from "@/components/layout";
import { Toaster } from "react-hot-toast";
import { cache } from "react";
import reference from "@/services/reference";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Finsweet",
  description: "Become the hero of your own story",
};

export const getSocialAccountsData = cache(async () => {
  const socialAccounts = await reference.socialAccounts();

  return {
    socialAccounts: socialAccounts,
  };
});

export default async function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const { socialAccounts } = await getSocialAccountsData();

  return (
    <html lang="en">
      <body className={inter.className}>
        <Toaster />
        <Header socialAccounts={socialAccounts} />
        <div className="relative mt-20">{children}</div>
      </body>
    </html>
  );
}
