import { Footer, Sponsers } from "@/components/layout";
import { Error404 } from "@/components/ui";
import { Author, LinstenNow } from "@/components/widgets";
import { PodcastProviderCard } from "@/components/widgets/cards";
import { RecentEpisodes } from "@/components/widgets/containers";
import { pageCache } from "@/services/page_cache";
import { renderFileUrl } from "@/utils";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

const SinglePodcastPage: FC<{
  params: { id: string };
}> = async ({ params: { id } }) => {
  const { detail, podcastAccounts, recentPodcasts } =
    await pageCache.podcastSinge(id);

  return (
    <>
      <main className="container mx-auto px-2">
        {detail && (
          <>
            <div className="md:min-h-[520px] flex flex-col-reverse md:flex-row justify-start md:py-16 items-stretch">
              <div className="flex-1 w-full min-h-[300px] relative">
                <Image
                  src={renderFileUrl(detail.cover_path)}
                  alt="hero"
                  fill
                  style={{ objectFit: "cover" }}
                />
              </div>
              <div className="flex-1 h-full flex flex-col gap-8 max-w-[768px] px-2 py-8 md:px-16 md:py-10">
                <div className="flex flex-col gap-4">
                  {detail.is_featured && (
                    <p className="caption text-primary">Featured</p>
                  )}
                  <h1 className="heading2">{detail.title}</h1>
                  <p>{detail.summary}</p>
                </div>

                <LinstenNow url={detail.audio_path} />
              </div>
            </div>
            <div className="flex flex-col-reverse md:flex-row gap-4 md:justify-between relative mt-10 max-w-[1060px] mx-auto">
              <div className="flex-1 w-full pr-10">
                <div
                  dangerouslySetInnerHTML={{ __html: detail.description_html }}
                ></div>
                <Author author={detail.author} />
              </div>
              <div className="w-[286px] flex flex-col gap-4 relative">
                <p className="heading3">Listen on</p>
                {detail.listen_ons?.map((item) => (
                  <Link key={item.name} href={item.url} target="_blank">
                    <PodcastProviderCard podcastAccount={item} />
                  </Link>
                ))}
              </div>
            </div>
          </>
        )}
        {!detail && (
          <>
            <Error404 />
          </>
        )}
        <RecentEpisodes podcasts={recentPodcasts} />
      </main>
      <Footer podcastProviders={podcastAccounts}>
        <Sponsers />
      </Footer>
    </>
  );
};

export default SinglePodcastPage;
