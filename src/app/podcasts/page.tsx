import { Footer, Sponsers } from "@/components/layout";
import { ContactUsCard } from "@/components/widgets/cards";
import { AllPodcasts, RecentEpisodes } from "@/components/widgets/containers";
import { pageCache } from "@/services/page_cache";
import { renderFileUrl } from "@/utils";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

const PodcastsPage: FC = async () => {
  const { featuredPodcast, podcastAccounts, recentPodcasts, categories } =
    await pageCache.podcasts();

  return (
    <>
      <main className="container mx-auto px-2">
        {featuredPodcast && (
          <>
            <div className="absolute -z-10 container md:h-[564px] mx-auto">
              <Image
                src={renderFileUrl(featuredPodcast.cover_path)}
                alt="hero"
                fill
                style={{ objectFit: "cover" }}
              />
            </div>
            {/* hero section */}
            <div className="md:h-[564px] flex justify-start items-start md:px-20 md:py-16 text-white">
              <div className="flex-1 w-full h-full flex flex-col gap-8 bg-primary max-w-[768px] border-l-[16px] border-secondary px-2 py-4 md:px-16 md:py-10">
                <div className="flex flex-col gap-4">
                  <p className="caption">Featured</p>
                  <h1 className="heading2 line-clamp-2">
                    {featuredPodcast.title}
                  </h1>
                  <p className="line-clamp-3">{featuredPodcast.summary}</p>
                </div>

                <Link href={`/podcasts/${featuredPodcast.id}`}>
                  <div className="flex gap-2 items-center cursor-pointer">
                    <Image
                      src={"/images/listen_secondary.png"}
                      alt="hero"
                      width={32}
                      height={32}
                    />
                    <p className="see-more">Listen Now</p>
                  </div>
                </Link>
              </div>
            </div>
          </>
        )}

        <RecentEpisodes hideSeeAll podcasts={recentPodcasts} />
        <AllPodcasts categories={categories} />
        <ContactUsCard bgImageSrc="/images/contact_us1.png" />
      </main>
      <Footer podcastProviders={podcastAccounts}>
        <Sponsers />
      </Footer>
    </>
  );
};

export default PodcastsPage;
