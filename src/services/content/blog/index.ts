import http from "@/services";
import { PaginationResponse } from "@/types";
import { Blog, BlogFilterInput } from "./types";

namespace blog {
  export const page = async (body: BlogFilterInput) =>
    http.post<PaginationResponse<Blog>>(`/guest/content/blog/page`, {
      body,
    });
  export const get = async (id: string | number) =>
    http.get<Blog>(`/guest/content/blog/get/${id}`, {});
}

export default blog;
