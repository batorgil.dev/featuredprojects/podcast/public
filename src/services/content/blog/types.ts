import { TeamMember } from "@/services/reference/types";
import { Base, PaginationRequest } from "@/types";

export interface BlogFilterInput extends PaginationRequest {
  author_id?: number;
  category_ids?: number[];
  created_at?: string[];
  description_html?: string;
  is_all?: boolean;
  is_featured?: string;
  published_at?: string[];
  summary?: string;
  title?: string;
}

export interface Blog extends Base {
  author?: TeamMember;
  cover_path: string;
  description_html: string;
  is_featured: boolean;
  published_at: string;
  summary: string;
  title: string;
  view_count: number;
  categories?: { id: number }[];
}
