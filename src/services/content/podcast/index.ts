import http from "@/services";
import { PaginationResponse } from "@/types";
import { Podcast, PodcastFilterInput } from "./types";
import { renderFileUrl } from "@/utils";

namespace podcast {
  export const page = async (body: PodcastFilterInput) =>
    http.post<PaginationResponse<Podcast>>(`/guest/content/podcast/page`, {
      body,
    });
  export const get = async (id: string | number) =>
    http.get<Podcast>(`/guest/content/podcast/get/${id}`, {});

  export const getAudioAsBlob = async (url: string) => {
    const resp = await fetch(renderFileUrl(url));
    return await resp.blob();
  };
}

export default podcast;
