import http from "@/services";
import { SuccessResponse } from "@/types";
import { SubscribeInput } from "./types";

namespace subscription {
  export const subscribe = async (body: SubscribeInput) =>
    http.post<SuccessResponse>(`/guest/subscription/subscribe`, {
      body,
    });

  export const unSubscribe = async (email: string) =>
    http.get<SuccessResponse>(
      `/guest/subscription/unsubscribe?email=${encodeURIComponent(email)}`,
      {}
    );
}

export default subscription;
