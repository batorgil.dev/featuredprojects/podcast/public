export interface ContactCreateInput {
  email: string;
  fullname: string;
  message: string;
  title: string;
}
