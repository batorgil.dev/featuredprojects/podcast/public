import http from "@/services";
import { ContactCreateInput } from "./types";
import { SuccessResponse } from "@/types";

namespace contact {
  export const request = async (body: ContactCreateInput) =>
    http.post<SuccessResponse>(`/guest/contact/request`, {
      body,
    });
}

export default contact;
