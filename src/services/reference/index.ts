import http from "@/services";
import {
  Category,
  PodcastAccount,
  Review,
  SocialAccount,
  Sponser,
  Statistic,
  TeamMember,
} from "./types";

namespace reference {
  export const categories = async () =>
    http.get<Category[]>(`/guest/reference/categories`, {});

  export const podcastAccounts = async () =>
    http.get<PodcastAccount[]>(`/guest/reference/podcast_accounts`, {});

  export const reviews = async () =>
    http.get<Review[]>(`/guest/reference/reviews`, {});

  export const socialAccounts = async () =>
    http.get<SocialAccount[]>(`/guest/reference/social_accounts`, {});

  export const sponsers = async () =>
    http.get<Sponser[]>(`/guest/reference/sponsers`, {});

  export const statistics = async () =>
    http.get<Statistic>(`/guest/reference/statistics`, {});

  export const team = async () =>
    http.get<TeamMember[]>(`/guest/reference/team`, {});
}

export default reference;
