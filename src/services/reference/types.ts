import { Base } from "@/types";

export interface Category extends Base {
  name: string;
}

export interface PodcastAccount {
  logo_path: string;
  name: string;
  url: string;
}

export interface Review {
  avatar_path: string;
  comment: string;
  fullname: string;
  star: number;
}

export interface SocialAccount {
  logo_path: string;
  name: string;
  url: string;
}

export interface Sponser {
  website: string;
  logo_path: string;
}

export interface Statistic {
  total_blog_views: number;
  total_podcast_views: number;
  total_podcasts: number;
  total_subscribe: number;
}

export interface TeamMember {
  avatar_path: string;
  bio: string;
  first_name: string;
  last_name: string;
  position: string;
  social_accounts: SocialAccount[];
}
