import { cache } from "react";
import reference from "./reference";
import podcast from "./content/podcast";
import { Blog } from "./content/blog/types";
import blog from "./content/blog";
import { Podcast } from "./content/podcast/types";

export namespace pageCache {
  export const about = cache(async () => {
    const podcastAccounts = await reference.podcastAccounts();
    const statistics = await reference.statistics();
    const team = await reference.team();
    const sponsers = await reference.sponsers();
    const recentPodcasts = await podcast.page({
      page: 0,
      limit: 3,
      sorter: { published_at: "DESC" },
    });

    return {
      podcastAccounts: podcastAccounts,
      recentPodcasts: recentPodcasts.items,
      statistics: statistics,
      team: team,
      sponsers: sponsers,
    };
  });

  export const blogSingle = cache(async (id: string) => {
    const podcastAccounts = await reference.podcastAccounts();

    let detail: undefined | Blog = undefined;
    try {
      detail = await blog.get(id);
    } catch (error) {}

    const relatedBlogs = await blog.page({
      page: 0,
      limit: 2,
      category_ids: detail?.categories
        ? detail?.categories.map((item) => item.id)
        : undefined,
      sorter: { published_at: "DESC" },
    });

    return {
      podcastAccounts: podcastAccounts,
      relatedBlogs: relatedBlogs.items,
      detail: detail,
    };
  });

  export const blogs = cache(async () => {
    const podcastAccounts = await reference.podcastAccounts();
    const featuredBlogs = await blog.page({
      page: 0,
      limit: 1,
      is_featured: "1",
      sorter: { published_at: "DESC" },
    });

    return {
      podcastAccounts: podcastAccounts,
      featuredBlog: featuredBlogs.items?.[0],
    };
  });

  export const contact = cache(async () => {
    const podcastAccounts = await reference.podcastAccounts();
    const socialAccounts = await reference.socialAccounts();

    return {
      podcastAccounts: podcastAccounts,
      socialAccounts: socialAccounts,
    };
  });

  export const host = cache(async () => {
    const podcastAccounts = await reference.podcastAccounts();
    const statistics = await reference.statistics();
    const recentPodcasts = await podcast.page({
      page: 0,
      limit: 3,
      sorter: { published_at: "DESC" },
    });

    return {
      podcastAccounts: podcastAccounts,
      recentPodcasts: recentPodcasts.items,
      statistics: statistics,
    };
  });

  export const podcastSinge = cache(async (id: string) => {
    const podcastAccounts = await reference.podcastAccounts();

    const recentPodcasts = await podcast.page({
      page: 0,
      limit: 3,
      sorter: { published_at: "DESC" },
    });

    let detail: undefined | Podcast = undefined;
    try {
      detail = await podcast.get(id);
    } catch (error) {}

    return {
      podcastAccounts: podcastAccounts,
      recentPodcasts: recentPodcasts.items,
      detail: detail,
    };
  });

  export const podcasts = cache(async () => {
    const podcastAccounts = await reference.podcastAccounts();
    const categories = await reference.categories();
    const featuredPodcasts = await podcast.page({
      page: 0,
      limit: 1,
      is_featured: "1",
      sorter: { published_at: "DESC" },
    });
    const recentPodcasts = await podcast.page({
      page: 0,
      limit: 3,
      sorter: { published_at: "DESC" },
    });

    return {
      categories: categories,
      podcastAccounts: podcastAccounts,
      recentPodcasts: recentPodcasts.items,
      featuredPodcast: featuredPodcasts.items?.[0],
    };
  });

  export const home = cache(async () => {
    const reviews = await reference.reviews();
    const podcastAccounts = await reference.podcastAccounts();
    const recentPodcasts = await podcast.page({
      page: 0,
      limit: 3,
      sorter: { published_at: "DESC" },
    });
    const recentBlogs = await blog.page({
      page: 0,
      limit: 2,
      sorter: { published_at: "DESC" },
    });

    return {
      reviews: reviews,
      recentPodcasts: recentPodcasts.items,
      podcastAccounts: podcastAccounts,
      recentBlogs: recentBlogs.items,
    };
  });
}
