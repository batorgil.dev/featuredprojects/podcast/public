export interface StringInterface {
  [x: string]: string;
}

export interface AnyInterface {
  [x: string]: any;
}

export type Optional<T> = T | undefined | null;

export interface Base {
  id: number;
  created_at: Date;
  updated_at: Date;
}

export interface BaseResponse<T> {
  message: string;
  body: T;
}

export interface PaginationResponse<T> {
  total: number;
  items: Array<T>;
}

export interface PaginationRequest {
  limit?: number;
  page?: number;
  sorter?: Optional<any>;
  is_all?: boolean;
}

export interface BaseFilter {
  created_at?: string[];
}

export type SuccessResponse = { success: boolean };

export const IsBoolEnum = {
  "1": {
    text: "True",
  },
  "0": {
    text: "False",
  },
};

export const ActiveBool = {
  "0": {
    text: "In-active",
  },
  "1": {
    text: "Active",
  },
};
