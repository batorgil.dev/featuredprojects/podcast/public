"use client";
import subscription from "@/services/subscription";
import { SubscribeInput } from "@/services/subscription/types";
import { useRequest } from "ahooks";
import Image from "next/image";
import { FC } from "react";
import { useForm } from "react-hook-form";
import { LoadingAnimation } from "../ui";
import toast from "react-hot-toast";

const Subscription: FC = () => {
  const { register, handleSubmit, reset } = useForm<SubscribeInput>();

  const subscribeAPI = useRequest(subscription.subscribe, {
    manual: true,
    onSuccess: () => {
      reset();
      toast.success(`You now subscribed`);
    },
  });

  return (
    <div
      className="w-full flex bg-primary relative min-h-[310px] overflow-hidden items-center justify-center"
      id="subscription"
    >
      <div className="absolute -bottom-0 opacity-10">
        <Image
          src={"/svgs/audio.svg"}
          width={3454}
          height={221}
          style={{ objectFit: "cover" }}
          alt="audio spectrum"
        />
      </div>

      <div className="absolute top-[50%] translate-y-[-50%] left-32 hidden lg:block">
        <Image
          src={"/images/subscription.png"}
          width={114}
          height={114}
          style={{ objectFit: "cover" }}
          alt="audio spectrum"
        />
      </div>

      <div className="absolute top-[50%] right-32 rotate-45 hidden lg:block">
        <Image
          src={"/images/subscription.png"}
          width={60}
          height={60}
          style={{ objectFit: "cover" }}
          alt="audio spectrum"
        />
      </div>

      <form
        onSubmit={handleSubmit((values) => {
          subscribeAPI.run(values);
        })}
      >
        <div className="flex flex-col gap-4">
          <h2 className="heading2 text-white p-4">
            Receive new episodes in your inbox.
          </h2>
          <div className="flex flex-col md:flex-row px-4">
            <input
              placeholder="Enter Your Email Id"
              type="email"
              {...register("email")}
              className="min-h-12 bg-offWhite px-4 flex-1"
            />
            <button
              className="btn-primary bg-secondary heading3 text-primary gap-2"
              type="submit"
              disabled={subscribeAPI.loading}
            >
              {subscribeAPI.loading && (
                <LoadingAnimation className="bg-primary" />
              )}
              <div>Subscribe</div>
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

export default Subscription;
