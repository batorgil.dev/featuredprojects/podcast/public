import reference from "@/services/reference";
import { renderFileUrl } from "@/utils";
import Image from "next/image";
import Link from "next/link";
import { FC, cache } from "react";

export const getSponsersData = cache(async () => {
  const sponsers = await reference.sponsers();

  return {
    sponsers: sponsers,
  };
});

const Sponsers: FC = async () => {
  const { sponsers } = await getSponsersData();

  return (
    <div className="w-full bg-primary text-white min-h-[286px] relative mx-auto p-4 md:p-24">
      <div className="flex justify-center">
        <h2 className="heading2">Our Sponsors</h2>
      </div>
      <div className="grid gap-4 grid-cols-2 md:grid-cols-3 lg:grid-cols-5 mt-9">
        {sponsers.map((item) => (
          <Link key={item.website} href={item.website} target="_blank">
            <div className="relative flex justify-center">
              <Image
                src={renderFileUrl(item.logo_path)}
                alt="sponser"
                width={172}
                height={34}
              />
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default Sponsers;
