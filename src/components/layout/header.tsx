"use client";

import { SocialAccount } from "@/services/reference/types";
import { renderFileUrl } from "@/utils";
import Image from "next/image";
import Link from "next/link";
import { FC, useState } from "react";

const Header: FC<{ socialAccounts: SocialAccount[] }> = ({
  socialAccounts,
}) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  return (
    <header className="w-full backdrop-blur-md fixed z-10 top-0 bg-white/50">
      <div className="container mx-auto flex flex-row items-center justify-between min-h-20 px-2">
        <Link href={"/"}>
          <Image src={"/images/logo.png"} alt="logo" width={130} height={26} />
        </Link>
        <div className="flex flex-row">
          <div className="hidden md:flex gap-6 mr-2">
            <Link className="header-item" href="/podcasts">
              Podcasts
            </Link>
            <Link className="header-item" href="/host">
              Host
            </Link>
            <Link className="header-item" href="/about">
              About
            </Link>
            <Link className="header-item" href="/blogs">
              Blog
            </Link>
            <Link className="header-item" href="/contact">
              Contact
            </Link>
          </div>
          <div className="flex md:hidden gap-6 mr-2">
            <div
              onClick={() => {
                setIsOpen(!isOpen);
              }}
            >
              {isOpen ? (
                <Image
                  className="cursor-pointer"
                  src={"/svgs/x.svg"}
                  alt="menu"
                  width={25}
                  height={25}
                />
              ) : (
                <Image
                  className="cursor-pointer"
                  src={"/svgs/menu.svg"}
                  alt="menu"
                  width={25}
                  height={25}
                />
              )}
            </div>
          </div>
          <div className="flex flex-row items-center gap-4">
            {socialAccounts.map((item) => (
              <div key={item.name}>
                <Link href={item.url} target="_blank">
                  <Image
                    src={renderFileUrl(item.logo_path)}
                    alt="social_logo"
                    width={25}
                    height={25}
                  />
                </Link>
              </div>
            ))}
          </div>
        </div>
      </div>
      {isOpen && (
        <div className="flex flex-col gap-4 md:hidden p-4">
          <Link href="/podcasts">Podcasts</Link>
          <Link href="/host">Host</Link>
          <Link href="/about">About</Link>
          <Link href="/blog">Blog</Link>
          <Link href="/contact">Contact</Link>
        </div>
      )}
    </header>
  );
};

export default Header;
