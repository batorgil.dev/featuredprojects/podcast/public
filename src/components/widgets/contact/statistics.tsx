"use client";
import { FormErrorItem, LoadingAnimation } from "@/components/ui";
import contact from "@/services/contact";
import { ContactCreateInput } from "@/services/contact/types";
import { useRequest } from "ahooks";
import { FC } from "react";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";

const ContactForm: FC = () => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<ContactCreateInput>();

  const contactAPI = useRequest(contact.request, {
    manual: true,
    onSuccess: () => {
      reset();
      toast.success(`You request is sent`);
    },
  });

  return (
    <div className="mt-8 ">
      <form
        className="flex flex-col gap-4"
        onSubmit={handleSubmit((values) => {
          contactAPI.run(values);
        })}
      >
        <div>
          <input
            placeholder="Your full Name"
            {...register("fullname", { required: "Fullname is required." })}
            className="min-h-12 bg-offWhite px-4 flex-1 w-full"
          />
          <FormErrorItem message={errors?.fullname?.message} />
        </div>
        <div>
          <input
            placeholder="Your Email Id"
            type="email"
            {...register("email", { required: "Email is required." })}
            className="min-h-12 bg-offWhite px-4 flex-1 w-full"
          />
          <FormErrorItem message={errors?.email?.message} />
        </div>
        <div>
          <input
            placeholder="Query Related"
            {...register("title", { required: "Query related is required." })}
            className="min-h-12 bg-offWhite px-4 flex-1 w-full"
          />
          <FormErrorItem message={errors?.title?.message} />
        </div>
        <div>
          <textarea
            rows={4}
            placeholder="Message"
            {...register("message", { required: "Message is required." })}
            className="min-h-12 bg-offWhite px-4 flex-1 w-full py-2"
          />
          <FormErrorItem message={errors?.message?.message} />
        </div>
        <div className="pt-4">
          <button className="btn-primary gap-2" type="submit">
            {contactAPI.loading && (
              <LoadingAnimation className="bg-secondary" />
            )}
            <div>Contact now</div>
          </button>
        </div>
      </form>
    </div>
  );
};

export default ContactForm;
