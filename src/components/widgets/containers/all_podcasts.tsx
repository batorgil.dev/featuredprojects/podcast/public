"use client";
import { FC, useCallback, useEffect, useState } from "react";
import PodcastCard from "../cards/podcast_card";
import { Category } from "@/services/reference/types";
import classNames from "classnames";
import { Podcast } from "@/services/content/podcast/types";
import { useRequest } from "ahooks";
import podcast from "@/services/content/podcast";
import { LoadingAnimation, SkeltonLoader } from "@/components/ui";

const AllPodcasts: FC<{ categories: Category[] }> = ({ categories }) => {
  const [selectedCategory, setSelectedCategory] = useState<Category>();
  const [page, setPage] = useState<number>(0);
  const [podcasts, setPodcasts] = useState<Podcast[]>([]);
  const [isReached, setIsReached] = useState<boolean>(false);
  const podcastAPI = useRequest(
    () => {
      return podcast.page({
        page: page,
        limit: 6,
        category_ids: selectedCategory?.id ? [selectedCategory.id] : undefined,
        sorter: { published_at: "desc" },
      });
    },
    {
      onSuccess: (res) => {
        const podcastsTmp = [...podcasts, ...res.items];
        if (podcastsTmp.length === res.total) setIsReached(true);
        setPodcasts(podcastsTmp);
      },
      debounceWait: 300,
      refreshDeps: [page, selectedCategory],
    }
  );

  useEffect(() => {
    setPage(0);
    setPodcasts([]);
    setIsReached(false);
  }, [selectedCategory]);

  return (
    <div className="flex flex-col mt-24 gap-8">
      <div className="flex flex-col md:flex-row  justify-between md:items-end gap-4">
        <div className="flex flex-col gap-2">
          <h1 className="heading2">All Podcasts</h1>
          <div className="flex flex-row gap-4 mt-4">
            <button
              className={classNames(
                "btn-rounded",
                !selectedCategory
                  ? "bg-primary text-white"
                  : "bg-white text-grey border-grey border-2"
              )}
              onClick={() => {
                setSelectedCategory(undefined);
              }}
            >
              All Podcasts
            </button>
            {categories.map((category) => (
              <button
                className={classNames(
                  "btn-rounded",
                  selectedCategory?.id == category.id
                    ? "bg-primary text-white"
                    : "bg-white text-grey border-grey border-2"
                )}
                key={category.id}
                onClick={() => {
                  setSelectedCategory(category);
                }}
              >
                {category.name}
              </button>
            ))}
          </div>
        </div>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
        {podcasts.map((item) => (
          <PodcastCard key={item.id} podcast={item} />
        ))}

        {podcastAPI.loading && (
          <>
            <SkeltonLoader />
            <SkeltonLoader />
            <SkeltonLoader />
            <SkeltonLoader />
            <SkeltonLoader />
            <SkeltonLoader />
          </>
        )}
      </div>
      {!isReached && (
        <div className="flex justify-center">
          <div>
            <button
              className="flex-1 btn-primary gap-2"
              type="submit"
              onClick={() => {
                setPage(page + 1);
              }}
              disabled={podcastAPI.loading}
            >
              {podcastAPI.loading && (
                <LoadingAnimation className="bg-secondary" />
              )}
              <div>Load more</div>
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default AllPodcasts;
