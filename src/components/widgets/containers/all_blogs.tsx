"use client";
import { LoadingAnimation, SkeltonLoader } from "@/components/ui";
import blog from "@/services/content/blog";
import { Blog } from "@/services/content/blog/types";
import { useRequest } from "ahooks";
import { FC, useState } from "react";
import { BlogCard } from "../cards";

const AllBlogs: FC = () => {
  const [page, setPage] = useState<number>(0);
  const [blogs, setBlogs] = useState<Blog[]>([]);
  const [isReached, setIsReached] = useState<boolean>(false);
  const blogAPI = useRequest(
    () => {
      return blog.page({
        page: page,
        limit: 6,
        sorter: { published_at: "desc" },
      });
    },
    {
      onSuccess: (res) => {
        const podcastsTmp = [...blogs, ...res.items];
        if (podcastsTmp.length === res.total) setIsReached(true);
        setBlogs(podcastsTmp);
      },
      debounceWait: 300,
      refreshDeps: [page],
    }
  );

  return (
    <div className="flex flex-col mt-24 gap-8">
      <div className="flex flex-col md:flex-row  justify-between md:items-end gap-4">
        <div className="flex flex-col gap-2">
          <h1 className="heading2">All Blogs</h1>
        </div>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
        {blogs.map((item) => (
          <BlogCard key={item.id} blog={item} />
        ))}

        {blogAPI.loading && (
          <>
            <SkeltonLoader />
            <SkeltonLoader />
            <SkeltonLoader />
            <SkeltonLoader />
            <SkeltonLoader />
            <SkeltonLoader />
          </>
        )}
      </div>
      {!isReached && (
        <div className="flex justify-center">
          <div>
            <button
              className="flex-1 btn-primary gap-2"
              type="submit"
              onClick={() => {
                setPage(page + 1);
              }}
              disabled={blogAPI.loading}
            >
              {blogAPI.loading && <LoadingAnimation className="bg-secondary" />}
              <div>Load more</div>
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default AllBlogs;
