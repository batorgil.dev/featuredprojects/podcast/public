import Link from "next/link";
import BlogCard from "../cards/blog_card";
import Image from "next/image";
import { FC } from "react";
import { Blog } from "@/services/content/blog/types";

const RelatedBlogs: FC<{ blogs?: Blog[] }> = ({ blogs = [] }) => {
  return (
    <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
      <div className="flex flex-col bg-offWhite">
        <div className="flex flex-col gap-4 p-4">
          <div>
            <h2 className="heading2">Read our</h2>
            <h2 className="heading2">articles & news</h2>
          </div>
          <Link href="/blogs" className="text-primary">
            See more
          </Link>
        </div>

        <div className="h-[320px] relative w-full">
          <Image
            src={"/images/news.png"}
            alt="hero"
            fill
            style={{ objectFit: "cover" }}
          />
        </div>
      </div>
      {blogs.map((blog) => (
        <BlogCard key={blog.id} blog={blog} />
      ))}
    </div>
  );
};

export default RelatedBlogs;
