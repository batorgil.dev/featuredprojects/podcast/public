import { FC } from "react";
import PodcastCard from "../cards/podcast_card";
import Link from "next/link";
import { Podcast } from "@/services/content/podcast/types";

const RecentEpisodes: FC<{ podcasts?: Podcast[]; hideSeeAll?: boolean }> = ({
  podcasts = [],
  hideSeeAll = false,
}) => {
  return (
    <div className="flex flex-col mt-24 gap-8">
      <div className="flex flex-col md:flex-row  justify-between md:items-end gap-4">
        <div className="max-w-[372px] flex flex-col gap-2">
          <h1 className="heading2">Recent Episodes</h1>
          <p>
            A podcast&apos;s main goal is to inform, entertain, and build
            community around engaging topics, fostering audience connection and
            impact.
          </p>
        </div>
        {!hideSeeAll && (
          <div>
            <Link href={"/podcasts"}>
              <button className="btn-primary">See All Episiodes</button>
            </Link>
          </div>
        )}
      </div>
      <div className="grid grid-cols-1 md:grid-cols-3 gap-4">
        {podcasts.map((item) => (
          <PodcastCard key={item.id} podcast={item} />
        ))}
      </div>
    </div>
  );
};

export default RecentEpisodes;
