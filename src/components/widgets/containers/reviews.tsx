"use client";
import { Review } from "@/services/reference/types";
import Image from "next/image";
import React, { FC } from "react";
import { ReviewCard } from "../cards";
import Slider from "react-slick";

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const Reviews: FC<{ reviews: Review[] }> = ({ reviews = [] }) => {
  const slider = React.useRef<Slider>(null);
  return (
    <div className="flex flex-col mt-24 gap-8">
      <div className="flex flex-col md:flex-row  justify-between md:items-end gap-4">
        <div className="max-w-[500px] flex flex-col gap-2">
          <h1 className="heading2">What our listeners say</h1>
          <p>
            A podcast dedicated to listeners focuses on delivering tailored
            content, engaging with feedback, and building a community,
            prioritizing audience needs and interests.
          </p>
        </div>
        <div className="flex gap-8 relative">
          <Image
            src={"/images/leftarrow.png"}
            alt="left"
            className="cursor-pointer"
            width={48}
            height={48}
            onClick={() => {
              slider?.current?.slickPrev();
            }}
          />
          <Image
            src={"/images/rightarrow.png"}
            alt="rigth"
            className="cursor-pointer"
            width={48}
            height={48}
            onClick={() => {
              slider?.current?.slickNext();
            }}
          />
        </div>
      </div>
      <Slider
        ref={slider}
        dots={false}
        infinite
        speed={500}
        slidesToShow={3}
        slidesToScroll={1}
        responsive={[
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
            },
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              initialSlide: 1,
            },
          },
        ]}
      >
        {reviews.map((review) => {
          return <ReviewCard key={review.fullname} review={review} />;
        })}
      </Slider>
    </div>
  );
};

export default Reviews;
