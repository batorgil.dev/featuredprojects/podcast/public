import { TeamMember } from "@/services/reference/types";
import { renderFileUrl, renderFullname } from "@/utils";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

const Author: FC<{ author?: TeamMember }> = ({ author }) => {
  if (author)
    return (
      <div className="w-full flex flex-col md:flex-row px-10 py-3 border-l-[16px] border-primary mt-16 md:items-center gap-8">
        <div className="w-[180px] flex flex-col gap-4">
          <div className="w-[64px] h-[64px] rounded-full overflow-hidden relative">
            <Image
              src={renderFileUrl(author.avatar_path)}
              alt="hero"
              fill
              style={{ objectFit: "cover" }}
            />
          </div>
          <div>
            <div className="font-bold text-[24px] line-clamp-1">
              {renderFullname(author)}
            </div>
            <div className="line-clamp-1">{author.position}</div>
          </div>
          <div className="flex flex-row items-center gap-4">
            {author.social_accounts?.map((item) => (
              <div key={item.name}>
                <Link href={item.url} target="_blank">
                  <Image
                    src={renderFileUrl(item.logo_path)}
                    alt="social_logo"
                    width={25}
                    height={25}
                  />
                </Link>
              </div>
            ))}
          </div>
        </div>
        <div className="flex-1 w-full">{author.bio}</div>
      </div>
    );

  return null;
};

export default Author;
