"use client";
import { Statistic } from "@/services/reference/types";
import { numberToUnit } from "@/utils";
import Image from "next/image";
import { FC, useMemo } from "react";
import CountUp from "react-countup";

const HostStatistics: FC<{ statistics: Statistic }> = ({ statistics }) => {
  const statisticsWithUnit = useMemo(() => {
    return {
      total_views: numberToUnit(
        statistics.total_blog_views + statistics.total_podcast_views
      ),
      total_blog_views: numberToUnit(statistics.total_blog_views),
      total_podcasts: numberToUnit(statistics.total_podcasts),
      total_subscribe: numberToUnit(statistics.total_subscribe),
    };
  }, [statistics]);

  return (
    <div className="grid grid-cols-2 lg:grid-cols-4 mt-32 mb-12 gap-4">
      <div className="w-full bg-primary p-1 md:p-8 text-white relative lg:top-10">
        <div className="relative p-4 w-full flex justify-center">
          <Image
            src={"/svgs/podcasts.svg"}
            width={128}
            height={128}
            alt="podcasts"
          />
        </div>
        <p className="heading1">
          <CountUp end={statisticsWithUnit.total_podcasts.value} />
          {statisticsWithUnit.total_podcasts.unit}
        </p>
        <p className="heading2">Podcasts</p>
      </div>
      <div className="w-full bg-primary p-1 md:p-8 text-white relative lg:-top-10">
        <div className="relative p-4 w-full flex justify-center">
          <Image
            src={"/svgs/view.svg"}
            width={128}
            height={128}
            alt="podcasts"
          />
        </div>
        <p className="heading1">
          <CountUp end={statisticsWithUnit.total_views.value} />
          {statisticsWithUnit.total_views.unit}
        </p>
        <p className="heading2">Views</p>
      </div>
      <div className="w-full bg-primary p-1 md:p-8 text-white relative lg:top-10">
        <div className="relative p-4 w-full flex justify-center">
          <Image
            src={"/svgs/subscribe.svg"}
            width={100}
            height={128}
            alt="podcasts"
          />
        </div>
        <p className="heading1">
          <CountUp end={statisticsWithUnit.total_subscribe.value} />
          {statisticsWithUnit.total_subscribe.unit}
        </p>
        <p className="heading2">Subscribers</p>
      </div>
      <div className="w-full bg-primary p-1 md:p-8 text-white relative lg:-top-10">
        <div className="relative p-4 w-full flex justify-center">
          <Image
            src={"/svgs/books.svg"}
            width={128}
            height={128}
            alt="podcasts"
          />
        </div>
        <p className="heading1">
          <CountUp end={statisticsWithUnit.total_blog_views.value} />
          {statisticsWithUnit.total_blog_views.unit}
        </p>
        <p className="heading2">Views</p>
      </div>
    </div>
  );
};

export default HostStatistics;
