import { PodcastAccount } from "@/services/reference/types";
import Link from "next/link";
import { FC } from "react";
import { PodcastProviderCard } from ".";

const AvailableOn: FC<{ podcastAccounts?: PodcastAccount[] }> = ({
  podcastAccounts = [],
}) => {
  return (
    <div className="flex flex-col md:flex-row md:items-center gap-4 md:justify-between relative mt-10">
      {/* podcasts */}
      <div
        className="flex-1"
        style={{
          fontWeight: 700,
          fontSize: 24,
        }}
      >
        Podcast Available On
      </div>
      {podcastAccounts.map((podcastAccount) => {
        return (
          <Link
            key={podcastAccount.name}
            href={podcastAccount.url}
            target="_blank"
            className="relative md:flex-1"
          >
            <PodcastProviderCard
              podcastAccount={podcastAccount}
              className="md:justify-end"
            />
          </Link>
        );
      })}
    </div>
  );
};

export default AvailableOn;
