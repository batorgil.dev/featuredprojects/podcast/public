import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

const ContactUsCard: FC<{ bgImageSrc: string }> = ({ bgImageSrc }) => {
  return (
    <div className="flex flex-col w-full min-h-[586px] relative mt-24">
      <div className="absolute w-full h-full -z-10">
        <Image
          src={bgImageSrc}
          fill
          style={{ objectFit: "cover" }}
          alt="contact"
        />
        <div className="absolute bottom-0 right-0">
          <Image src="/images/pulse.png" width={571} height={146} alt="pulse" />
        </div>
      </div>
      <div className="w-full p-4 md:p-24 text-white">
        <div className="max-w-[440px] flex flex-col gap-8">
          <p className="heading1">Become The Hero Of Your Own Story</p>
          <p className="">
            Introduce FinSweet as a platform dedicated to helping entrepreneurs
            and business professionals achieve success.
          </p>
          <div>
            <Link href={"/contact"}>
              <button className="border-white outline outline-1 h-[36px] px-6">
                Contact Us
              </button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContactUsCard;
