import { Sponser } from "@/services/reference/types";
import { renderFileUrl } from "@/utils";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

const SponserCard: FC<{ sponser: Sponser }> = ({ sponser }) => {
  return (
    <Link href={sponser.website} target="_blank">
      <div className="flex flex-col justify-center items-center h-[224px] bg-offWhite">
        <div className="relative">
          <Image
            className="invert"
            src={renderFileUrl(sponser.logo_path)}
            alt="sponser"
            width={172}
            height={34}
          />
        </div>
      </div>
    </Link>
  );
};

export default SponserCard;
