"use client";
import { Podcast } from "@/services/content/podcast/types";
import { renderFileUrl } from "@/utils";
import Image from "next/image";
import { useRouter } from "next/navigation";
import { FC } from "react";

const PodcastCard: FC<{ podcast: Podcast }> = ({ podcast }) => {
  const router = useRouter();

  const goToPodcast = () => {
    router.push(`/podcasts/${podcast.id}`);
  };
  return (
    <div className="flex-1 flex flex-col gap-2">
      <div
        className="min-h-[256px] relative w-full cursor-pointer"
        onClick={goToPodcast}
      >
        <Image
          src={renderFileUrl(podcast.cover_path)}
          alt="hero"
          fill
          style={{ objectFit: "cover" }}
        />
        <div className="absolute top-4 right-4">
          <Image src={"/images/mic.png"} alt="mic" width={40} height={40} />
        </div>
      </div>
      <h2 className="heading3 line-clamp-1">{podcast.title}</h2>
      <p className="line-clamp-2">{podcast.summary}</p>
      <button className="flex gap-2 items-center" onClick={goToPodcast}>
        <Image src={"/images/listen.png"} alt="hero" width={32} height={32} />
        <p className="text-primary see-more">Listen Now</p>
      </button>
    </div>
  );
};

export default PodcastCard;
