"use client";
import { Blog } from "@/services/content/blog/types";
import { renderFileUrl } from "@/utils";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { FC } from "react";

const BlogCard: FC<{ blog: Blog }> = ({ blog }) => {
  const router = useRouter();
  return (
    <div className="flex-1 flex flex-col gap-2">
      <div
        className="min-h-[320px] relative w-full cursor-pointer"
        onClick={() => {
          router.push(`/blogs/${blog.id}`);
        }}
      >
        <Image
          src={renderFileUrl(blog.cover_path)}
          alt="hero"
          fill
          style={{ objectFit: "cover" }}
        />
      </div>
      <h2 className="heading3 line-clamp-1">{blog.title}</h2>
      <p className="line-clamp-2">{blog.summary}</p>
      <div className="flex gap-2 items-center">
        <Link className="text-primary see-more" href={`/blogs/${blog.id}`}>
          Read Now
        </Link>
      </div>
    </div>
  );
};

export default BlogCard;
