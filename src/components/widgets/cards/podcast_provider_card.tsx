import { PodcastAccount } from "@/services/reference/types";
import { renderFileUrl } from "@/utils";
import classNames from "classnames";
import Image from "next/image";
import { FC } from "react";

const PodcastProviderCard: FC<{
  podcastAccount: PodcastAccount;
  className?: string;
}> = ({ podcastAccount, className }) => {
  return (
    <div className={classNames("flex flex-row items-center gap-2", className)}>
      <div className="h-[36px] relative w-[36px]">
        <Image
          src={renderFileUrl(podcastAccount.logo_path)}
          alt="hero"
          fill
          style={{ objectFit: "contain" }}
        />
      </div>
      <div className="heading4">{podcastAccount.name}</div>
    </div>
  );
};

export default PodcastProviderCard;
