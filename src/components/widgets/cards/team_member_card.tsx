import { TeamMember } from "@/services/reference/types";
import { renderFileUrl, renderFullname } from "@/utils";
import Image from "next/image";
import Link from "next/link";
import { FC } from "react";

const TeamMemberCard: FC<{ teamMember: TeamMember }> = ({ teamMember }) => {
  return (
    <div className="flex flex-col gap-2">
      <div className="h-[300px]">
        <div className="relative w-full h-full max-w-[300px] max-h-[300px] overflow-hidden bg-dark">
          <Image
            src={renderFileUrl(teamMember.avatar_path)}
            alt="avatar"
            fill
            style={{ objectFit: "cover" }}
          />
        </div>
      </div>

      <div>
        <div className="heading2">{renderFullname(teamMember)}</div>
        <div className="text-grey">{teamMember.position}</div>
      </div>
      <div className="flex flex-row items-center gap-4">
        {teamMember.social_accounts?.map((item) => (
          <div key={item.name}>
            <Link href={item.url} target="_blank">
              <Image
                src={renderFileUrl(item.logo_path)}
                alt="social_logo"
                width={25}
                height={25}
              />
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
};

export default TeamMemberCard;
