"use client";
import { Review } from "@/services/reference/types";
import { renderFileUrl } from "@/utils";
import Image from "next/image";
import { FC } from "react";

const ReviewCard: FC<{ review: Review }> = ({ review }) => {
  return (
    <div className="flex flex-col gap-4 p-4">
      <div className="flex-1 flex gap-4 items-center">
        <div className="rounded-full overflow-hidden w-[80px] h-[80px] relative">
          <Image
            src={renderFileUrl(review.avatar_path)}
            alt="hero"
            fill
            style={{ objectFit: "cover" }}
          />
        </div>
        <div className="flex flex-col gap-1">
          <p>{review.fullname}</p>
          <div className="flex gap-1">
            {Array.from({ length: review.star }, (_, index) => {
              return (
                <Image
                  key={index}
                  src={"/images/star.png"}
                  alt="star"
                  width={20}
                  height={20}
                />
              );
            })}
          </div>
        </div>
      </div>
      <p>{review.comment}</p>
      <div className="w-full mt-4 h-[12px] bg-primary" />
    </div>
  );
};

export default ReviewCard;
