"use client";
import { LoadingAnimation } from "@/components/ui";
import subscription from "@/services/subscription";
import { SubscribeInput } from "@/services/subscription/types";
import { useRequest } from "ahooks";
import Image from "next/image";
import { FC } from "react";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";

const HeroSection: FC = () => {
  const { register, handleSubmit, reset } = useForm<SubscribeInput>();

  const subscribeAPI = useRequest(subscription.subscribe, {
    manual: true,
    onSuccess: () => {
      reset();
      toast.success(`You now subscribed`);
    },
  });

  return (
    <div className="h-[60vh] flex flex-col md:flex-row gap-16 max-w-[1024px] items-center mx-auto">
      <div className="max-w-[486px] w-full flex flex-col gap-8 m-auto">
        <h1 className="heading1 max-w-[440px]">
          Become The Hero Of Your Own Story
        </h1>
        <p>
          Introduce FinSweet as a platform dedicated to helping entrepreneurs
          and business professionals achieve success.
        </p>
        <form
          onSubmit={handleSubmit((values) => {
            subscribeAPI.run(values);
          })}
        >
          <div className="flex flex-col md:flex-row">
            <input
              placeholder="Enter Your Email Id"
              type="email"
              {...register("email")}
              className="min-h-12 bg-offWhite px-4 flex-1"
            />
            <button
              className="flex-1 btn-primary gap-2"
              type="submit"
              disabled={subscribeAPI.loading}
            >
              {subscribeAPI.loading && (
                <LoadingAnimation className="bg-secondary" />
              )}
              <div>Subscribe</div>
            </button>
          </div>
        </form>
      </div>
      <div className="relative flex-1 h-[256px] max-w-[432px] w-full">
        {/* image */}
        <Image
          src={"/images/hero.png"}
          alt="hero"
          fill
          style={{ objectFit: "contain" }}
        />
      </div>
    </div>
  );
};

export default HeroSection;
