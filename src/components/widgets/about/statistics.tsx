"use client";
import { Statistic } from "@/services/reference/types";
import { numberToUnit } from "@/utils";
import Image from "next/image";
import { FC, useMemo } from "react";
import CountUp from "react-countup";

const AboutStatistics: FC<{ statistics: Statistic }> = ({ statistics }) => {
  const statisticsWithUnit = useMemo(() => {
    return {
      total_views: numberToUnit(
        statistics.total_blog_views + statistics.total_podcast_views
      ),
      total_blog_views: numberToUnit(statistics.total_blog_views),
      total_podcasts: numberToUnit(statistics.total_podcasts),
      total_subscribe: numberToUnit(statistics.total_subscribe),
    };
  }, [statistics]);

  return (
    <div className="grid grid-cols-1 md:grid-cols-3 gap-8 mt-24 p-2">
      <div className="flex flex-col gap-4">
        <div className="relative w-[80px] h-[80px] overflow-hidden">
          <Image
            src={"/images/about_us1.png"}
            alt="icon"
            fill
            style={{ objectFit: "contain" }}
          />
        </div>
        <div className="heading4">
          <CountUp end={statisticsWithUnit.total_podcasts.value} />
          {statisticsWithUnit.total_podcasts.unit} Podcast
        </div>
        <div>
          Podcasts offer convenience, education, and entertainment, empowering
          listeners to learn and stay informed while multitasking or on-the-go.
        </div>
      </div>
      <div className="flex flex-col gap-4">
        <div className="relative w-[80px] h-[80px] overflow-hidden">
          <Image
            src={"/images/about_us2.png"}
            alt="icon"
            fill
            style={{ objectFit: "contain" }}
          />
        </div>
        <div className="heading4">
          <CountUp end={statisticsWithUnit.total_views.value} />
          {statisticsWithUnit.total_views.unit} Views
        </div>
        <div>
          High view counts indicate popularity, audience engagement, potential
          for monetization, and increased visibility and credibility for content
          creators.
        </div>
      </div>
      <div className="flex flex-col gap-4">
        <div className="relative w-[80px] h-[80px] overflow-hidden">
          <Image
            src={"/images/about_us3.png"}
            alt="icon"
            fill
            style={{ objectFit: "contain" }}
          />
        </div>
        <div className="heading4">
          <CountUp end={statisticsWithUnit.total_subscribe.value} />
          {statisticsWithUnit.total_subscribe.unit} Subs
        </div>
        <div>
          Subscriptions ensure regular access to new episodes, foster a loyal
          audience base, and provide metrics for audience engagement and growth.
        </div>
      </div>
    </div>
  );
};

export default AboutStatistics;
