"use client";
import { FC, useEffect, useLayoutEffect, useRef, useState } from "react";
import AudioVisualizer from "../ui/audio_visualizer";
import { useInterval, useRequest, useToggle } from "ahooks";
import podcast from "@/services/content/podcast";
import { renderFileUrl } from "@/utils";

const LinstenNow: FC<{ url: string }> = ({ url }) => {
  const ref = useRef<HTMLDivElement>(null);
  const audioRef = useRef<HTMLAudioElement>(null);
  const [playing, { toggle }] = useToggle(false);
  const [width, setWidth] = useState<number>(0);
  const [currentTime, setCurrentTime] = useState<number>(0);
  const visualizerRef = useRef<HTMLCanvasElement>(null);
  const audioAsBlob = useRequest(() => podcast.getAudioAsBlob(url), {});

  useLayoutEffect(() => {
    setWidth(ref.current?.offsetWidth || 0);
  }, []);

  const stop = useInterval(() => {
    if (audioRef.current) {
      setCurrentTime(audioRef.current.currentTime);
    }
  }, 300);

  useEffect(() => {
    return () => {
      stop();
    };
  }, [stop]);

  return (
    <>
      <audio
        ref={audioRef}
        src={renderFileUrl(url)}
        controls={false}
        onEnded={() => {
          if (audioRef.current) {
            toggle();
            audioRef.current.pause();
            audioRef.current.currentTime = 0;
          }
        }}
      />
      <div className="flex gap-2 items-center bg-gray-100 rounded-md h-[80px] relative p-2">
        <div>
          <button
            className="w-[48px] h-[48px] flex items-center justify-center border-2 border-primary rounded-full"
            onClick={async () => {
              if (!playing) {
                await audioRef.current?.play();
              } else {
                await audioRef.current?.pause();
              }
              toggle();
            }}
          >
            {playing ? (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 22 22"
                width="22"
                style={{
                  width: "18px",
                }}
                className="fill-primary"
              >
                <path
                  d="M22 1v20h-1v1h-7v-1h-1V1h1V0h7v1h1zM8 1h1v20H8v1H1v-1H0V1h1V0h7v1z"
                  style={{
                    width: "18px",
                  }}
                  className="fill-primary"
                ></path>
              </svg>
            ) : (
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 22"
                width="22"
                style={{
                  width: "18px",
                }}
                className="fill-primary"
              >
                <path
                  d="M20 10v2h-1v1h-1v1h-2v1h-2v1h-1v1h-2v1H9v1H8v1H6v1H4v1H1v-1H0V1h1V0h3v1h2v1h2v1h1v1h2v1h2v1h1v1h2v1h2v1h1v1h1z"
                  className="fill-primary"
                  style={{
                    width: "18px",
                  }}
                ></path>
              </svg>
            )}
          </button>
        </div>

        <div
          ref={ref}
          className="flex-1 cursor-pointer"
          onClick={(e) => {
            if (ref.current) {
              const bcr = ref.current.getBoundingClientRect();
              const percentage = (e.clientX - bcr.left) / bcr.width;
              if (audioRef.current) {
                const newCurrent = audioRef.current.duration * percentage;
                audioRef.current.currentTime = newCurrent;
              }
            }
          }}
        >
          {audioAsBlob.data && (
            <AudioVisualizer
              ref={visualizerRef}
              blob={audioAsBlob.data}
              width={width}
              height={60}
              barWidth={1}
              barColor="#503AE7"
              barPlayedColor="#1AD993"
              currentTime={currentTime}
              gap={0}
            />
          )}
        </div>
      </div>
    </>
  );
};

export default LinstenNow;
