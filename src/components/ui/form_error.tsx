import { FC } from "react";

const FormErrorItem: FC<{ message?: string }> = ({ message }) => {
  return !!message ? (
    <div className="px-1 text-red-500 font-light">{message}</div>
  ) : null;
};

export default FormErrorItem;
